
# use whatever dns provider you have to configure the A record for your homeserver
# we use cloudflare

locals {
  # this contains the public IP you want to make an A record for in your DNS provider
  # it is the ONLY dns record you need for this entire setup
  jitsi_master_public_ip = module.jitsi_master_eu.elastic_ip.public_ip
}

resource "cloudflare_record" "jitsi_master_eu" {
  zone_id = data.sops_file.secrets.data["cloudflare_zone_id"]
  name    = data.sops_file.secrets.data["jitsi_home_server"]
  value   = local.jitsi_master_public_ip
  type    = "A"
}
