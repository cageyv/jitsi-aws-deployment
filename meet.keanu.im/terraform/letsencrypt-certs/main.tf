terraform {
  required_version = ">= 0.12"

  backend "s3" {
    key = "letsencrypt-certs/terraform.tfstate"
  }
  required_providers {
    sops = "~> 0.5"
  }
}

provider "tls" {}

provider "acme" {
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

resource "tls_private_key" "acme_account" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}

resource "acme_registration" "production" {
  account_key_pem = tls_private_key.acme_account.private_key_pem
  email_address   = data.sops_file.secrets.data.letsencrypt_email
}

resource "acme_certificate" "production" {
  account_key_pem           = tls_private_key.acme_account.private_key_pem
  common_name               = local.common_name
  subject_alternative_names = local.dns_names
  depends_on                = [acme_registration.production]
  recursive_nameservers     = ["1.1.1.1:53"]

  min_days_remaining = local.force_renewal ? 999 : 30

  dns_challenge {
    # change to your supported provider:
    # https://www.terraform.io/docs/providers/acme/dns_providers/index.html
    provider = "cloudflare"

    config = {
      CF_DNS_API_TOKEN               = local.cloudflare_dns_api_token
      CF_ZONE_API_TOKEN              = local.cloudflare_zone_api_token
      CLOUDFLARE_PROPAGATION_TIMEOUT = 600
    }
  }
}
