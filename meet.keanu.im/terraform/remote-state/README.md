# remote-state

This is a terraform root module for bootstrapping the terraform state backend
in an AWS account.

It only needs to be run once to bootstrap the terraform state bucket for this account.


```
make setup
```

The first time it is run it will create the state locally, then import it into the bucket.

Finally, it will create `../backend.tfvars` which are loaded by the other root modules.
