module "label_main_us1" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.16.0"
  namespace  = local.namespace
  stage      = local.stage
  name       = local.name
  attributes = [local.aws_region, local.us1_region]
  tags       = local.tags
}

module "vpc_peer_main_us1" {
  source = "git::https://github.com/grem11n/terraform-aws-vpc-peering.git?ref=tags/v2.2.3"

  providers = {
    aws.this = aws
    aws.peer = aws.us1
  }

  this_vpc_id = local.vpc_ids.main
  peer_vpc_id = local.vpc_ids.us1

  auto_accept_peering = true
  peer_dns_resolution = true
  this_dns_resolution = true

  tags = module.label_main_us1.tags
}
