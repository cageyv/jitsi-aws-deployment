output "vpc_india" {
  value = module.vpc_india
}

output "public_subnets_india" {
  value = module.public_subnets_india
}

output "vpc_us1" {
  value = module.vpc_us1
}

output "public_subnets_us1" {
  value = module.public_subnets_us1
}
output "vpc_all_cidr_blocks" {
  value = [
    data.terraform_remote_state.vpc_main.outputs.vpc.vpc_cidr_block,
    local.india_public_cidr_block,
    local.us1_public_cidr_block,
  ]
}

