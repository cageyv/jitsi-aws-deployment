output "instance_id" {
  value       = aws_instance.jitsi_master.id
  description = "the ID of the EC2 instance"
}

output "elastic_ip" {
  value       = aws_eip.elastic_ip
  description = "the elastic ip output attached to the instance"
}

output "common_ssm_prefix" {
  value       = local.common_ssm_prefix
  description = "the ssm param store path prefix this jitsi master stored the common shared params under"
}

output "self_ssm_prefix" {
  value       = local.self_ssm_prefix
  description = "the ssm param store path prefix this jitsi master stored its own params under"
}

output "ssm_kms_key_arn" {
  value       = module.kms_key.key_arn
  description = "the kms key arn used to encrypt/decrypt the ssm param store items"
}

output "jitsi_master_association" {
  value = aws_ssm_association.jitsi_master
}
