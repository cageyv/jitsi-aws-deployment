output "instance_id" {
  value       = aws_instance.jitsi_jvb.id
  description = "The ID of the EC2 instance"
}

output "elastic_ip" {
  value       = aws_eip.elastic_ip
  description = "The elastic ip output attached to the instance"
}
