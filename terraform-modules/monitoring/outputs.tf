output "asg" {
  value = module.autoscale_group
}

#output "persistent_ebs" {
#  value = module.persistent_ebs
#}

output "efs_kms_key" {
  value = module.kms_key
}

#output "init_snippet_ebs_volume" {
#  value = module.init_snippet_ebs_volume
#}
output "efs" {
  value = aws_efs_file_system.default
}

output "efs_mount_target" {
  value = aws_efs_mount_target.default
}
